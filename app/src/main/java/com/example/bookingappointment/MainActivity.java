package com.example.bookingappointment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    //private Spinner spinner;
    private EditText phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        phoneNumber=findViewById(R.id.phoneNumber);
        phoneNumber.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

        findViewById(R.id.otp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Code="91";
                String number = phoneNumber.getText().toString().trim();

                if(number.isEmpty() || number.length() < 10){
                    phoneNumber.setError("Number is required");
                    phoneNumber.requestFocus();
                    return;
                }

                String phone = "+" + Code + number;

                Intent intent = new Intent(MainActivity.this,VerifyPhoneActivity.class);
                intent.putExtra("PhoneNumber",phone);
                ContextCompat.startForegroundService(MainActivity.this,intent);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(FirebaseAuth.getInstance().getCurrentUser() != null){
            Intent intent = new Intent(this,ProfileActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            ContextCompat.startForegroundService(MainActivity.this,intent);
            startActivity(intent);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseAuth.getInstance().signOut();
    }
}
